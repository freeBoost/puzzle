﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameMechanics : ILogic {

	public static MonoBehaviour coroutine;
	private ICard target;
	private ICard lastTarget;
	private List<Sprite> sprits;
	//private List<ICard> cardsPrefab;
	private List<ICard> field;
	private int couples;
	private int numberCard;
	private int repetitions;
	private GameObject modelPrefa;
	private LavelState state;
	private int collectedPairs;

	private List<Vector2> fieldPosotion;
	private List<TypeImage> typeImage;
	private IEnumerator compare;

	const float WAIT_START_SHIRT = 5f;
	const float WAIT_HIDE_CARD = 1f;
	const float WAIT_SEC = 1f;

	// ленивая инициализация 
	public void Initialization () {
		// задаём позицию карт 
		fieldPosotion = new List<Vector2>();
		fieldPosotion.Add( new Vector2(-2.5f, 0f ));
		fieldPosotion.Add( new Vector2(0, 0f ));
		fieldPosotion.Add( new Vector2(2.5f, 0f ));
		fieldPosotion.Add( new Vector2(-2.5f, 3.25f ));
		fieldPosotion.Add( new Vector2(0, 3.25f ));
		fieldPosotion.Add( new Vector2(2.5f, 3.25f ));	
		field = new List<ICard>();
		typeImage = new List<TypeImage>();
		//cardsPrefab = new List<ICard>();
		repetitions = 2;
		collectedPairs = 0;
		// создаем тип карты и её спрайт  
		int maxType = 3; 
		for (int i = 0; i < maxType; i++)
		{
			typeImage.Add(new TypeImage(i, sprits[i]));
		}
	}

	public void Start () {
		ICard card;
		numberCard = couples * 2;
		// создаём карты
		foreach (var item in typeImage)
		{
			for (int i = 0; i < repetitions; i++) //количество дубликатов
			{
				card = Card.Creater(modelPrefa, item.sprite, CardState.FACE, item.typeCard);
				field.Add(card);					
				card.AddHadler(CardHandler);
			}
		}
		// перемешиваем
		//int count = 0;
		List<ICard> fieldRandom = field;
		// если не забуду, то напишу нормальный
		// System.Random rand = new System.Random();
		// fieldRandom = fieldRandom.OrderBy( (x) => rand.Next() ).ToList();
		//Random RND = new Random();
		ICard _card;
		ICard _cardRandom;
		for (int i = 0; i < fieldRandom.Count; i++)
		{
			int random = Random.Range(0, fieldRandom.Count);
			_card = fieldRandom[0];
			_cardRandom = fieldRandom[random];
			fieldRandom[0] = _cardRandom;
			fieldRandom[random] = _card;
		}
		for (int i = 0; i < fieldRandom.Count; i++)
		{
			fieldRandom[i].SetPosition( fieldPosotion[i] );
		}
		coroutine.StartCoroutine(IEStart());
	}

	public void Restart () {
		field = new List<ICard>();
		collectedPairs = 0;
		Start();
	}

	public void Stop () {

	}

	IEnumerator IEStart () {
		yield return new WaitForSeconds(WAIT_START_SHIRT);
		foreach (var item in field)
		{
			item.SetState( CardState.BACKSIDE);
		}
		yield return new WaitForSeconds(WAIT_SEC);
		this.state = LavelState.PLAYER;
	}

	IEnumerator IECompare () {
		state = LavelState.WAIT;
		bool check = target.CompareType(lastTarget);
		if (check) {
			yield return new WaitForSeconds(WAIT_SEC);
			target.SetActive(false);
			lastTarget.SetActive(false);
			collectedPairs++;
			Game.Event("CoupleCollected");
			target = null;
			lastTarget = null;
		}
		else
		{
			yield return new WaitForSeconds(WAIT_SEC);
			target.SetState(CardState.BACKSIDE);
			lastTarget.SetState(CardState.BACKSIDE);
			target = null;
			lastTarget = null;		
		}
		yield return new WaitForSeconds(WAIT_SEC);
		if ( Win() ) {
			Restart();
		}
		else
		{
			state = LavelState.PLAYER;
		}
	}

	void CardHandler (ICard card, CardEventArgs e) {
		if (e.message == "Pick" && state == LavelState.PLAYER) {
			target = card;
			if (lastTarget == null) {
				lastTarget = target;
				lastTarget.SetState(CardState.FACE);
				target = null;
			}
			else if (lastTarget != target) {
				target.Play("Face");
				compare = IECompare ();
				coroutine.StartCoroutine(compare);
			}
		}
	}

	bool Win () {
		int magicNumber = 3;
		if (collectedPairs == magicNumber) {
			return true;
		}
		else
		{
			return false;
		}
	}

	public static GameMechanics Creater (MonoBehaviour coroutineMono, List<Sprite> sprits, int couples, GameObject model) {
		GameMechanics logic = new GameMechanics ();
		coroutine = coroutineMono;
		logic.modelPrefa = model;
		logic.sprits = sprits;
		logic.couples = couples;
		logic.state = LavelState.WAIT;
		return logic;
	}
}

// переименовать и вынести отдельным cs
public struct TypeImage
{
	public int typeCard;
	public Sprite sprite;
	public TypeImage (int typeCard, Sprite sprite) {
		this.typeCard = typeCard;
		this.sprite = sprite;
	}
}
