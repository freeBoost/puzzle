﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILogic {

	void Initialization ();
	void Start ();
	void Restart ();
	void Stop ();
}
