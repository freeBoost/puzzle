﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICountCollectedPairs : MonoBehaviour {

	private Text text;
	private int points;
	
	void Awake () {
		text = GetComponent<Text>();
		points = 0;
		text.text = "points: " + points;
		Game.AddEvent(ADDHandler);
	}

	public void ADDHandler (GameEventArgs e) {
		if (e.message == "CoupleCollected") {
			points++;
			text.text = "points: " + points;
		}
	}
}
