﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventArgs : EventArgs {

	public GameEventArgs (string message) : base(message) {
	}
}