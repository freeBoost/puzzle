﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class GameEvent : UnityEvent<GameEventArgs> {}

public delegate void gameHandler (GameEventArgs e);
