﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// ленивый контроллер

	private RaycastHit2D rayHit;

	void Start () {
		
	}
	
	void Update () {
		TapCursor();
	}

	private void TapCursor() {
		if (Input.GetKeyDown(KeyCode.Mouse0)) {
			GameObject selected = GetGameObjectTapCursorr();
			if (selected != null) {
				selected.GetComponent<ModelComponent>().owner.Selected();
			}
		}
	}

	private GameObject GetGameObjectTapCursorr() {
		GameObject target;
		Vector2 CurMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		rayHit = Physics2D.Raycast(CurMousePos, Vector2.zero);
		if (rayHit.collider !=null) {
			target = rayHit.collider.gameObject;
			return target;
		}
		target = null;
		return target;
	}		
}
