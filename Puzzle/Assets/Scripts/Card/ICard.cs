﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICard {

	void AddHadler (cardHandler handler);
	void Selected ();
	void SetPosition (Vector2 position);
	void SetState (CardState state);
	void SetActive(bool value);
	Vector2 GetPosition ();
	CardState GetState ();
	int GetTypeX ();
	bool CompareType (ICard card);
	void Play (string animation);
	ICard Clone ();

}
