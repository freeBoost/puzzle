﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : CardBase {

	public Card () {}

	public Card (IModel model, Vector3 position, CardState state) {
		base.model = model;
		base.position = position;
		base.state = state;
	}

	public static bool CompareType (ICard cardA, ICard cardB) {
		int cardTypeA = cardA.GetTypeX();
		int cardTypeB = cardB.GetTypeX();
		if (cardTypeA == cardTypeB) {
			return true;
		}
		else { return false; }
	}

	public override ICard Clone () {
		Card clone = new Card();
		clone.cardEvent = new CardEvent();
		clone.type = this.type;
		//clone.model = Model.Creater();
		clone.position = this.position;
		clone.state = this.state;
		return clone;
	}

	public static Card Creater (GameObject model, Sprite face, CardState state, int type) {
		Card created = new Card ();
		created.cardEvent = new CardEvent();
		created.active = true;
		created.model =  new Model(model, face, created);
		//created.position = position;
		created.state = state;
		created.type = type;

		return created;
	}
}
