﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model : IModel {

	public ICard card;
	public GameObject model;
	public GameObject face;
	public GameObject shirt;
	public Sprite sprite;
	public Animator animator;

	void SetPositionHandler (ICard card, CardEventArgs e) {
		if (e.message == "NewPosition") {
			model.transform.position = card.GetPosition();
		}
	}

	void SetActiveHandler (ICard card, CardEventArgs e) {
		if (e.message == "SetActiveTrue") {
			model.SetActive(true);
		}
		else if (e.message == "SetActiveFalse")
		{
			model.SetActive(false);
		}
	}

	void PlayHandler (ICard card, CardEventArgs e) {
		// пусть будет так 
		switch (e.message)
		{
			case "Face"   :
				animator.Play(e.message);
                break;
			case "Shirt"  :
				animator.Play(e.message);
                break;
			default: break;
		}
	}

	public void Play (string animation) {
		animator.Play(animation);
	}

	public Model (GameObject model, Sprite face, ICard card) {

		this.model = GameObject.Instantiate(model); 
		this.animator = this.model.GetComponent<Animator>();
		this.face = this.model.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
		this.shirt = this.model.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject;
		this.face.GetComponent<SpriteRenderer>().sprite = face;
		this.card = card;
		this.model.GetComponent<ModelComponent>().owner = this.card;
		this.card.AddHadler(this.SetPositionHandler);
		this.card.AddHadler(this.PlayHandler);
		this.card.AddHadler(this.SetActiveHandler);
	}

	/*
	public static Model Creater (GameObject model, Sprite face, ICard card) {
		Model created = new Model ();
		created.model = GameObject.Instantiate(model); 
		created.animator = created.model.GetComponent<Animator>();
		created.face = model.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
		created.shirt = model.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject;
		created.face.GetComponent<SpriteRenderer>().sprite = face;
		created.card = card;
		created.card.AddHadler(created.SetPositionHandler);
		created.card.AddHadler(created.PlayHandler);
		return created;
	}
	*/
}
