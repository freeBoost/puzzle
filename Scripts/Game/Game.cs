﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game {

	private static GameEvent gameEvent = new GameEvent();
	
	public static void AddEvent (gameHandler handler) {
		gameEvent.AddListener(handler.Invoke);
	}

	public static void Event (string message) {
		gameEvent.Invoke(new GameEventArgs (message));
	}
}
