﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class LevelComponent : MonoBehaviour {

	// settigs level
	[Header("Settigs level")]
	public string urlJson; // https://drive.google.com/uc?export=download&id=1eqb72PT7ZjFLFVHyHMHMxitIPoSSCk4U
	public int couples;
	public GameObject model;

	// initialization level
	public List<Sprite> sprits;
	private ILogic logic;

	// no internet Texture
	public Texture2D A;
	public Texture2D B;
	public Texture2D C;
	
	public bool IsInternet;

	private void Awake () {
		Initialization();
		StartCoroutine("IEInitialization");
	}

	private void Initialization () {
		sprits = new List<Sprite>();
	}

	IEnumerator IEInitialization () {
		if (IsInternet) {
			// можно было бы сделать проверку на соединение с интернетом и исключение, но и так сойдёт :D
			WWW www = new WWW(urlJson);
			yield return www;
			string b = www.text;
			var json = JObject.Parse(b);
			Rect rect = model.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite.rect;
			int npictures = 3;
			// переделать 
			for (int i = 0; i < npictures; i++)
			{
				// эх, можно ещё было бы добавить анимацию загрузки 
				var url = json["images"][i]["picture"].Value<string>();
				www = new WWW(url);
				yield return www;
				Sprite created = Sprite.Create(www.texture, rect, new Vector2 (0.5f, 0.5f) );
				sprits.Add(created);		
			}
		}
		else
		{
			// мб сделаю через лист и перечисление 
			Rect rect = model.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite.rect;
			Sprite created = Sprite.Create(A, rect, new Vector2 (0.5f, 0.5f) );
			sprits.Add(created);
			created = Sprite.Create(B, rect, new Vector2 (0.5f, 0.5f) );
			sprits.Add(created);
			created = Sprite.Create(C, rect, new Vector2 (0.5f, 0.5f) );
			sprits.Add(created);				
		}
		logic = GameMechanics.Creater(this, sprits, couples, model);
		logic.Initialization();
		logic.Start();
	}

	public ILogic GetLogic () {
		return this.logic;
	}
}