﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CardBase : ICard {

	protected CardEvent cardEvent;
	protected int type;
	protected IModel model;
	protected Vector2 position;
	protected CardState state; // хмм, или сделать отдельным классом 
	protected bool active;

	public void AddHadler (cardHandler handler) {
		cardEvent.AddListener(handler.Invoke);
	}

	public void Selected () {
		cardEvent.Invoke(this, new CardEventArgs("Pick"));
	}

	public void SetState (CardState state) {
		this.state = state;
		if (this.state == CardState.FACE) {
			cardEvent.Invoke(this, new CardEventArgs("Face"));
		}
		else if (this.state == CardState.BACKSIDE) {
			cardEvent.Invoke(this, new CardEventArgs("Shirt"));
		}
	}

	public void SetPosition (Vector2 position) {
		this.position = position;
		cardEvent.Invoke(this, new CardEventArgs("NewPosition"));
	}

	public bool CompareType (ICard card) {
		int cardType = card.GetTypeX();
		if (this.type == cardType) {
			return true;
		}
		else { return false; }		
	}

	public Vector2 GetPosition () {
		return this.position;
	}

	public CardState GetState () {
		return this.state;
	}

	public int GetTypeX () {
		return this.type;
	}

	public void Play (string animation) {
		model.Play(animation);
	}

	public void SetActive(bool value) {
		active = value;
		if (active) {
			cardEvent.Invoke(this, new CardEventArgs("SetActiveTrue"));
		}
		else
		{
			cardEvent.Invoke(this, new CardEventArgs("SetActiveFalse"));
		}
	}

	public abstract ICard Clone ();
}