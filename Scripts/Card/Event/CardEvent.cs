﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CardEvent : UnityEvent<ICard, CardEventArgs> {}

public delegate void cardHandler (ICard player, CardEventArgs e);